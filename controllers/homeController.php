<?php
class homeController extends Controller{
	
	public function __construct(){
		$u = new Usuarios();
		
		if (!$u->isLogged()) {
			header("location: /twitter/login");
		} 
	}
	
	public function index(){
		$dados = array(
			'nome' => '',
		);

		$p = new Posts();

		if (isset($_POST['msg']) && !empty($_POST['msg'])) {
			$msg = addslashes($_POST['msg']);
			$p->inserirPost($msg);
		}
		
		//Neste caso a instancia do objeto já recebe os dados do usuário logado
		$u = new Usuarios($_SESSION['twlg']);
		$dados['nome'] = $u->getNome();
		$dados['qt_seguidos'] = $u->countSeguidos();
		$dados['qt_seguidores'] = $u->countSeguidores();
		$dados['sugestao'] = $u->getUsuarios(5);

		$lista = $u->getSeguidos();
		//Adicionando meu usuário a lista de posts do feed
		$lista[] = $_SESSION['twlg'];
		$dados['feed'] = $p->getFeed($lista, 10); 

		$this->loadTemplate('home', $dados);
	}

	public function seguir($id){
		if (!empty($id)) {
			$r = new Relacionamentos();
			$r->seguir($_SESSION['twlg'], $r->consultaSeguido($id));
		}
		header("location: /twitter");
	}

	public function deseguir($id){
		if (!empty($id)) {
			$r = new Relacionamentos();
			$r->deseguir($_SESSION['twlg'], $r->consultaSeguido($id));
		}
		header("location: /twitter");
	}


}