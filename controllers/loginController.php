<?php

class loginController extends Controller{
	
	public function index(){
		
		$dados = array('aviso' => '');

		if (isset($_POST['email']) && !empty($_POST['email'])) {
			
			$email = addslashes($_POST['email']);
			$senha = md5($_POST['senha']);

			$u = new Usuarios();

			if ($u->fazerLogin($email, $senha)) {
				header("location: /twitter");
			}else{
				$dados['aviso'] = "Seu login ou senha não conferem!";
			}
		}
		
		//Como é uma página com layout diferente não será chamado o template
		$this->loadView('login', $dados);
	}

	public function cadastro(){
		$dados = array('aviso' => '');

		if (isset($_POST['nome']) && !empty($_POST['nome'])) {
			$nome = addslashes($_POST['nome']);
			$email = addslashes($_POST['email']);
			$senha = md5($_POST['senha']);

			if (!empty($nome) && !empty($email) && !empty($senha)) {
				
				$u = new Usuarios();

				if (!$u->usuarioExiste($email)) {
					$_SESSION['twlg'] = $u->inserirUsuario($nome, $email, $senha);
					header("location: /twitter");
				}else{
					$dados['aviso'] = "Este usuário já existe!";
				}

			}else{
				$dados['aviso'] = "Preencha todos os campos!";
			}
		}

		$this->loadView('cadastro', $dados);
	}

	public function sair(){
		unset($_SESSION['twlg']);
		header("location: /twitter");
	}

}
	
