<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Twitter</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
	<div class="container-fluid" style="background-color: grey; ">
		<div class="row">
			<div class="col-md-10">
				<h1 class="text-center" style="color: white;">Twitter</h1>
			</div>
			<div class="col-md-2">
				<p class="lead"><?php echo $viewData['nome'] ; ?>
				<a href="/twitter/login/sair" type="button" class="btn btn-default">Sair</a></p>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		
			<div class="col-md-8" id="feed" style="background-color: white;">
				<form method="post">
					<textarea name="msg" style="width: 98%; height: 100px; margin-top: 1%; border: dashed; border-radius: 8px;"></textarea>
					<input type="submit" name="enviar" value="Publicar" class="btn btn-default">
				</form>
				<div style="margin-top: 4%;">
				<?php foreach ($feed as $item): ?>
					<b><?php echo $item['nome']; ?></b> <?php echo date('H:i', strtotime($item['data_post']));?></br>
					<?php echo $item['mensagem']; ?>
					<hr>
				<?php endforeach; ?>	
				</div>
			</div>
			
			<div class="col-md-4 hidden-sm hidden-xs" id="sidebar" style="background-color: #E3E3E5;">
				<div class="container row">
					<p class="lead">RELACIONAMENTOS</p>
					<div class="col-md-2">
						<p><b><?php echo $viewData['qt_seguidores']; ?></b> Seguidores</p>
					</div>
					<div class="col-md-2">
						<p><b><?php echo $viewData['qt_seguidos']; ?></b> Seguidos</p>
					</div>
				</div>
				<div class="container row">
					<p class="lead">SUGESTÕES DE AMIZADE</p>
					<div class="col-md-4">
					<table>
						<?php foreach ($sugestao as $usuario): ?>
						<tr>
							<td style="padding: 10px; padding-right: 140px;"><?php echo $usuario['nome']; ?></td>
							<?php if($usuario['seguido'] == '0'): ?>
							<td><a href="/twitter/home/seguir/<?php echo $usuario['id']; ?>">Follow</a></td>
							<?php else: ?>
							<td><a href="/twitter/home/deseguir/<?php echo $usuario['id']; ?>">Unfollow</a></td>
							<?php endif; ?>
						</tr>
						<?php endforeach; ?>
					</table>
					</div>
				</div>
			</div>
			
	</div>

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
 		<script src="Hello World"></script>
	</body>
</html>